This treatment can be used to test the limits of Sluice. 

Specifically, it has no data baked into the treatment that can be visualized by default. However, there are several scripts that exist inside which can be run. 

1. Entity testing on Vector fluoroscopes can be done via the live-data-generator.py file that exists in share. You can experiment with the entity count, and the update rate. Additionally, this script logs memory used by sluice on the system to an output file which can be charted with the chart.py script (also in the share folder). 

Note that to view these entities, you should create a fluoroscope that listens for the 'airport' kind. You can also experiment with the update_observations api call via the "update_my_observations" function in the script. If you do this, you will also have to include in the fluoroscope pigments that listen to the particular attibutes that are being updated

2. Web fluoro testing can be done via the test-web-fluoros.py script also in the share directory. Currently this inserts a number of fluoroscopes and loads in a new random webpage to each one every x seconds. The update frequency x and the number of web fluoroscopes can both be altered for the purposes of scientific exploration :). Lets see what Sluice can handle!

This script also logs sluice memory usage to an output file which can be charted via the same method described above.  
