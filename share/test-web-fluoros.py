from sluice import Sluice
import random
import time
import datetime
import subprocess
import re

UPDATE_RATE_S=1.0

def sluice_rss():
    pid = subprocess.Popen(['pgrep', 'sluice'], stdout=subprocess.PIPE).communicate()[0]
    #ps = subprocess.Popen(['ps', '-p', pid], stdout=subprocess.PIPE).communicate()[0]
    ps = subprocess.Popen(['ps', '-orss,comm'], stdout=subprocess.PIPE).communicate()[0]

    # Iterate processes
    processLines = ps.split('\n')
    sep = re.compile('[\s]+')
    sluiceRss = 0
    for row in range(1,len(processLines)):
        rowText = processLines[row].strip()
        rowElements = sep.split(rowText)
        try:
            rss = float(rowElements[0]) * 1024
            if (rowElements[1] == "sluice"):
                sluiceRss = rss
                break
        except:
            rss = 0 # ignore...
    return sluiceRss

def main():
    s = Sluice()
    logName = "live_data_mem_log_" + time.strftime("%Y%m%d-%H-%M-%S", time.gmtime()) + '.csv'
    f = open(logName,'w')
    f.close()
    urls = ['www.google.com','www.conduce.com','www.yahoo.com']
    while True: 
        t1 = time.clock()
        s.request_web_fluoro('1',random.choice(urls))
        s.request_web_fluoro('2',random.choice(urls))
        s.request_web_fluoro('3',random.choice(urls))
        t2 = time.clock()
        sleeptime = UPDATE_RATE_S - (t2 - t1) 
        stats = str(datetime.datetime.now()) + ',' + str(sluice_rss())
        f = open(logName,'a')
        f.write(stats + '\n')
        f.close()
        print stats
        time.sleep(sleeptime)


if __name__=="__main__":
    main()
