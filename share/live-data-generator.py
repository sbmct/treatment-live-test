"""Live data generator

Usage: live-data-generator.py [options]

Options:
    --continue, -c  Continue without initializing entities
    -e <entity_count>, --entity-count=<entity_count>  Number of entities to generate [default: 1000]
    -p <update_period>, --period=<update_period>  Time between updates [default: 1]
    --center-start  Start entities at center of US, otherwise entities will start in random US locations
    -s <decimal_degrees>, --step-size=<decimal_degrees>  Size of entity step offset in degrees (applied in latitude and longitude) [default: 0.001]
    -w, --march-west  All entities move west at same speed
    -q, --disable-stats  Disable statistics collection
    -t, --relative-time  Start the clock at epoch instead of now
    -g, --ungoverned  Ignore the update period, update nodes as quickly as possible
    -h <host>, --hostname=<host> Host name or IP address where sluice is running
    --without-topology  Stop topology updates from being published
    --with-observations  Enable publication of observations
    --stats-only  Do not actually inject data into sluice, just monitor performance

"""

from sluice import Sluice
from sluice.types import v3float64
import time
import datetime
import random
import copy

import subprocess
import re
import os
import sys

from docopt import docopt 

entities=[]
CENTER_OF_US=v3float64(float(39.8282), float(-98.5795))

def sluice_rss():
    pid = subprocess.Popen(['pgrep', 'sluice'], stdout=subprocess.PIPE).communicate()[0]
    if not pid:
        print "Sluice is not running, exiting..."
        sys.exit(1)

    ps = subprocess.Popen(['ps', '-p', str(int(pid)), '-orss,%cpu'], stdout=subprocess.PIPE).communicate()[0]
    total_memory = subprocess.Popen(['sysctl', 'hw.memsize'], stdout=subprocess.PIPE).communicate()[0]

    processLines = ps.split('\n')
    sep = re.compile('[\s]+')
    sluiceRss = 0
    percentCpu = 0
    rowText = processLines[1].strip()
    rowElements = sep.split(rowText)
    totalMemSplit = sep.split(total_memory.strip())
    try:
        sluiceRss = int(rowElements[0]) * 1024
        percentCpu = float(rowElements[1])
        percentMemory = sluiceRss / float(totalMemSplit[1]) * 100
    except:
        sluiceRss = 0 # ignore...
        percentCpu = 0
    return sluiceRss,percentMemory,percentCpu

def initialize_entities(arguments):
    timeNow = 0
    if not arguments.get('--relative-time'):
        if(float(arguments.get('--period')) < 1 or arguments.get('--ungoverned')):
            timeNow = time.time()
        else:
            timeNow = int(time.time())
    entityCount = int(arguments.get('--entity-count'))
    for i in range(entityCount):
        startLoc = copy.deepcopy(CENTER_OF_US)
        if not arguments.get('--center-start'):
            startLoc = v3float64(float(random.uniform(24,49)),float(random.uniform(-66,-125)),0.0)
        entity = {
                'id': i,
                'loc': startLoc, 
                'kind': 'airport',
                'timestamp': timeNow,
                'attrs': {
                    'startLoc': startLoc
                    }
                }
        entities.append(entity);

def get_updates(arguments):
    timeNow = entities[0]['timestamp'] + float(arguments.get('--period'))
    if not arguments.get('--relative-time'):
        if(float(arguments.get('--period')) < 1 or arguments.get('--ungoverned')):
            timeNow = time.time()
        else:
            timeNow = int(time.time())  #sluice does not update entities when "live" if using sub-second precision

    stepSize = float(arguments.get('--step-size'))

    x_delta = stepSize * -1
    y_delta = 0
    x = 1
    y = 0

    entityCount = int(arguments.get('--entity-count'))
    for i in range(entityCount):
        if not arguments.get('--march-west'):
            x_delta = float(random.uniform(-1000,1000)) / 1000. * stepSize
        y_delta = float(random.uniform(-1000,1000)) / 1000. * stepSize
        pre_y = entities[i]['loc'][y]
        pre_x = entities[i]['loc'][x]
        entities[i]['loc'][y] += y_delta
        entities[i]['loc'][x] += x_delta
        post_y = entities[i]['loc'][y]
        post_x = entities[i]['loc'][x]
        if(pre_y > -180 and post_y < -180):
            entities[i]['loc'][y] += 360
        elif(pre_y < 180 and post_y > 180):
            entities[i]['loc'][y] -= 360
        if(pre_x > -180 and post_x < -180):
            entities[i]['loc'][x] += 360
        elif(pre_x < 180 and post_x > 180):
            entities[i]['loc'][x] -= 360
        entities[i]['timestamp'] = timeNow 
    #print entities[0]['loc'][y], entities[0]['loc'][x]
    return entities

def get_observations(entities):
    for entity in entities:
        obs = entity.copy()
        del obs['loc']
        del obs['kind']
        yield obs

epoch = datetime.datetime.utcfromtimestamp(0)
def seconds_since_epoch(dt):
    return (dt - epoch).total_seconds()

def main():
    arguments = docopt(__doc__)
    UPDATE_PERIOD_S = float(arguments.get('--period'))
    COLLECT_STATS = not arguments.get('--disable-stats')
    UNGOVERNED = arguments.get('--ungoverned')
    PUBLISH_TOPOLOGY = not arguments.get('--without-topology')
    PUBLISH_OBSERVATIONS = arguments.get('--with-observations')
    MONITOR_ONLY = arguments.get('--stats-only')

    if MONITOR_ONLY:
        PUBLISH_TOPOLOGY = False
        PUBLISH_OBSERVATIONS = False
        COLLECT_STATS = True

    if arguments.get('--hostname'):
        print "Connecting to sluice on", arguments.get('--hostname')
        s = Sluice(arguments.get('--hostname'))
        COLLECT_STATS = False
    else:
        sluice_rss()
        s = Sluice()

    if arguments.get('--continue'):
        logName = 'current_live_log.csv'
    else:
        logName = "live_data_mem_log_" + time.strftime("%Y%m%d-%H-%M-%S", time.gmtime()) + '.csv'
        subprocess.Popen(['ln', '-sf', logName, 'current_live_log.csv'], stdout=subprocess.PIPE).communicate()[0]
        f = open(logName,'w')
        f.close()

    initialize_entities(arguments);
    if not arguments.get('--continue') and not MONITOR_ONLY:
        s.inject_topology(entities)

    counter = 0

    while True:
        t1 = time.clock()
        if not MONITOR_ONLY:
            get_updates(arguments)
        if PUBLISH_TOPOLOGY:
            s.update_topology(entities)
        if PUBLISH_OBSERVATIONS:
            s.update_observations(get_observations(entities))

        if COLLECT_STATS:
            stats = sluice_rss()
            line = str(seconds_since_epoch(datetime.datetime.now())) + ',' + str(stats[0]) + ',' + str(stats[1]) + ',' + str(stats[2])
            f = open(logName,'a')
            f.write(line + '\n')
            f.close()
            print line
            print counter
            counter += 1

        t2 = time.clock()
        iterationTime = t2 - t1

        if UNGOVERNED:
            print "p =", iterationTime
        else:
            sleeptime = UPDATE_PERIOD_S - iterationTime
            time.sleep(sleeptime)

if __name__=="__main__":
    main()
