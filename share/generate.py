import csv
import random

with open('test-data.csv','wb') as f:
    writer = csv.writer(f)
    for i in xrange(100000):
        code = random.randint(100000,999999)
        lat = random.uniform(20,90) 
        lon = random.uniform(-70,-120)
        writer.writerow([code,lat,lon])
