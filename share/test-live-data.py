from sluice import Sluice
from sluice.types import v3float64
import time
import csv
import random

def parse():
    test_data = []
    with open('test-data.csv','rb') as f:
        reader = csv.reader(f)
        for row in reader:
            if row:
                test_data.append(row[0])
    print test_data
    return test_data

def inject():
    record = {
        'id': 'test',
        'kind': 'test-kind',
        'loc': v3float64(float(37.3327498),float(-121.9012372), 0.0),
        'attrs': {'value':'0'}
    }
    return record

def update(A):
    data = []
    for i in range(50000):
        rand = A[random.randint(0,len(A)-1)]
        value = random.randint(1,3)     
        record = {
            'id': rand,
            'loc': v3float64(float(random.uniform(20,90)),float(random.uniform(-70,-120)),0.0),
            'kind': 'airport'
        }
        data.append(record)
    print data
    return data


def main():
    A = parse()
    s = Sluice()
#    s.inject_topology([inject()])
    while True:
        s.update_topology(update(A))
        #time.sleep(1)

if __name__=="__main__":
    main()
